import axios from "axios";

class users {
  static async getUsers() {
    try {
      const data = await axios.get("users");
      if (data.data.error) throw data.data;
      return data.data.users;
    } catch (e) {
      let data = { error: { msg: e } };
      return data;
    }
  }

  static async setUser(newUser) {
    try {
      const data = await axios.post("users", newUser);
      if (data.data.error) throw data.data;
      return data.data.users;
    } catch (e) {
      let data = { error: { msg: e } };
      return data;
    }
  }

  static async deleteUser(userId) {
    try {
      const data = await axios.delete("users", { data: userId });
      if (data.data.error) throw data.data;
      return data.data.users;
    } catch (e) {
      let data = { error: { msg: e } };
      return data;
    }
  }

  static async updateUser(_id, data) {
    const userData = [{ _id }, data];
    console.log(userData);
    try {
      const data = await axios.put("users", userData);
      if (data.data.error) throw data.data;
      return data.data.users;
    } catch (e) {
      let data = { error: { msg: e } };
      return data;
    }
  }
}

export default users;
