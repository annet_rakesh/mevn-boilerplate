class db {
  constructor(collection) {
    this.collection = collection;
    this.data;
  }

  get() {
    return new Promise((_resolve, _reject) => {
      this.collection.find((err, res) => {
        if (err || res.length == 0)
          return _reject({
            msg: "No record found"
          });
        return _resolve(res);
      });
    });
  }

  save() {
    return new Promise((_resolve, _reject) => {
      this.collection.create(this.data, (err, res) => {
        if (err || res.length == 0)
          return _reject({
            msg: "Unable to save data."
          });
        return _resolve(res);
      });
    });
  }

  delete() {
    return new Promise((_resolve, _reject) => {
      this.collection.deleteOne(this.data, (err, res) => {
        if (err || res.length == 0)
          return _reject({
            msg: "Fail to delete."
          });
        return _resolve(res);
      });
    });
  }

  /**
   * Need to work on it
   */
  update() {
    return new Promise((_resolve, _reject) => {
      this.collection.updateOne(this.data[0], this.data[1], (err, res) => {
        if (err || res.length == 0)
          return _reject({
            msg: "Fail to delete."
          });
        return _resolve(res);
      });
    });
  }

  load(data) {
    this.data = data;
    return this;
  }
}
module.exports = db;
