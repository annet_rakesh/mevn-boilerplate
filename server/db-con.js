var mongoose = require("mongoose");
mongoose
  .connect("mongodb://localhost:27017/test", { useNewUrlParser: true })
  .catch(error => handleError(error));
module.exports = mongoose;
