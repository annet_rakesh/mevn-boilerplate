const mongoose = require(__server+"db-con");
const db = require(__server+"db");

class usersModal{
    constructor() {
        const schema = { name: String, age: String, completed: Boolean };
        var userSchema = new mongoose.Schema(schema);
        var user = mongoose.model("users", userSchema);
        this.db = new db(user);
    }
}

module.exports = usersModal;