const userModal = require(__model+"users/users");

class users extends userModal {
  constructor() {
    super();
    this.userData;
  }
  async getUsers(req, res) {
    try {
      this.userData = await this.db.get();
      res.send({ users: this.userData });
    } catch (error) {
      error.msg
        ? res.send({ error })
        : res.send({ error: { msg: "Something went wrong" } });
    }
  }

  async setUsers(req, res) {
    const newUser = {
      name: req.body.name,
      age: req.body.age,
      completed: req.body.completed
    };
    try {
      await this.db.load(newUser).save();
      return this.getUsers(req, res);
    } catch (error) {
      error.msg
        ? res.send({ error })
        : res.send({ error: { msg: "Something went wrong" } });
    }
  }

  async removeUser(req, res) {
    try {
      const userId = req.body;
      await this.db.load(userId).delete();
      return this.getUsers(req, res);
    } catch (error) {
      error.msg
        ? res.send({ error })
        : res.send({
            error: { msg: "Something went wrong" }
          });
    }
  }

  /**
   * Update user
   * @param {Request from Vue} req
   * @param {Return} res
   */
  async updateUser(req, res) {
    try {
      await this.db.load(req.body).update();
      return this.getUsers(req, res);
    } catch (error) {
      error.msg
        ? res.send({ error })
        : res.send({
            error: { msg: "Something went wrong" }
          });
    }
  }
}

module.exports = users;
