var express = require("express");
var router = express.Router();
var users = require(__component+"users/users");
var users = new users();
/* GET users listing. */
router.get("/", function(req, res, next) {
  return users.getUsers(req, res);
});

router.post("/", function(req, res, next) {
  return users.setUsers(req, res);
});

router.delete("/", function(req, res, next) {
  return users.removeUser(req, res);
});

router.put("/", function(req, res, next) {
  return users.updateUser(req, res);
});
module.exports = router;
